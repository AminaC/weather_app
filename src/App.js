import React from 'react';
import Weather from "./weather";
import "./App.css";

const APP_KEY = "272251ac3fe4169bd15958a05cafad8e";

class App extends React.Component {

  // State
  state = {
    country: undefined,
    temp: undefined,
    humidity: undefined,
    description: undefined,
    windSpeed: undefined
  }

 getData = async (event) => {
    event.preventDefault();
    const city = event.target.city.value;
    //console.log(city);
    const response = await fetch(`https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${APP_KEY}`);
    const data = await response.json();
    if (city) {
      this.setState(
        {
          country : data.sys.country,
          temp: data.main.temp,
          humidity: data.main.humidity,
          description: data.weather[0].description,
          windSpeed: data.wind.speed
        }
      );
    }
    }

    onButtonClickHandler = () => {
      var x = document.getElementById("weather");
      x.classList.toggle("show");
      x.classList.toggle("hide");
    };

 render(){
   return (
     <div className="App">
      <div className="container">
        <form onSubmit={this.getData}>
          <input type="text" name="city" placeholder="Enter city name" className="input"/>
          <button type="submit" className="btn" onClick={this.onButtonClickHandler}>Get Weather</button>
        </form>

        <div className="weather hide" id="weather">
          <Weather
          country={this.state.country}
          temp={this.state.temp}
          humidity={this.state.humidity}
          description={this.state.description}
          windSpeed={this.state.windSpeed}/>
        </div>

       </div>
     </div>
   );
 }

}

export default App;
