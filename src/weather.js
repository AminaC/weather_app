import React from 'react';
import style from "./weather.module.css" ;

class Weather extends React.Component{
  render(){
    return(
      <div className="wrapper">
        <p>{this.props.country}</p>
        <p>{Math.round(this.props.temp - 273,15)} Celsius</p>
        <p>{this.props.humidity}%</p>
        <p>{this.props.description}</p>
        <p>{this.props.windSpeed} Meter/Sec</p>
      </div>
    );
  }

}

export default Weather;
